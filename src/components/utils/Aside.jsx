import React from "react";
import {Link} from 'react-router-dom'

export default function Aside() {
  return  (
    <aside className="w-1/6 bg-black h-screen">
        <ul className="text-white p-4">
            <Link to="/">
                <li className="bg-gray-900 py-1 px-3 rounded">
                    Home
                </li>
            </Link>
            <Link to="/profile">
                <li className="bg-gray-900 py-1 px-3 rounded">
                    Profile
                </li>
            </Link>
            <Link to="/books">
                <li className="bg-gray-900 py-1 px-3 rounded">
                    Books
                </li>
            </Link>
    </ul>
    </aside>
)
}