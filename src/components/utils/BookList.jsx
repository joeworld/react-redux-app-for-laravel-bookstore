import React from 'react';

export default function BookList({books}){
    console.log(books[0])
    if(books == null){
        return ""
    }
    return <tbody>
            {books && books.length > 0 && books.map((book, todoIndex) => {
                    return <React.Fragment key={book.data.id}>
                    <tr>
                        <td className="border px-4 py-2">{book.data.title}</td>
                        <td className="border px-4 py-2">{book.data.description}</td>
                        <td className="border px-4 py-2">{book.data.avg_review}</td>
                    </tr>
                </React.Fragment>
            })}
        </tbody>
}