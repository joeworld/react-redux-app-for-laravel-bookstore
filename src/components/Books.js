import React, { Component } from 'react'
import Aside from './utils/Aside'
import Endpoint from '../services/Endpoint'
import Success from './utils/Success';
import BookList from './utils/BookList';

export default class Books extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
        todos: "",
        errors: {},
        page: "",
        sortColumn: "",
        sortDirection: "",
        title: "",
        authors: "",
        result: "",
        books: {}
        }
    }

    handleForm = e => {
        e.preventDefault();
        const data = {
          page: this.state.page,
          sortColumn: this.state.sortColumn,
          sortDirection: this.state.sortDirection,
          title: this.state.title,
          authors: this.state.authors
        };

        const params = `?page=${data.page}&title=${data.title}&sortColumn=${data.sortColumn}&sortDirection=${data.sortDirection}&authors=${data.authors}`

        Endpoint.books(params)
        .then(res => {
          this.setState({result: 'Data Filtered Successfully', books: res.data})
          console.log(this.state.books);
        })
        .catch(e => this.setState({ errors: e.response.data.errors }));
      };

      handleInput = e => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
    };

    render() {
        return (
            <div className="flex w-full">
                <Aside></Aside>
                <section className="w-screen m-2 bg-white flex justify-center">
                    <form className="border border-gray-500 w-1/2 my-5 rounded"
                    onSubmit={this.handleForm}>
                        <div className="p-4">
                            <br />
                            <Success
                            msg={
                            this.state.result
                            ? this.state.result
                            : null
                            }
                            >
                            </Success>
                <br />
                <h1 className="text-lg border-b border-gray-500">
                Filter Books
                </h1>
                <div className="mt-4">
                    <label>FIlter by Text/Description/ISBN</label>
                    <input 
                        type="text"
                        name="title"
                        placeholder="FIlter by Text/Description/ISBN"
                        onChange={this.handleInput}
                        value={this.state.title}
                        className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                    />
                </div>
                <div className="mt-4">
                    <label>FIlter by Author IDs</label>
                    <input 
                        type="text"
                        name="authors"
                        placeholder="FIlter by Author Ids"
                        onChange={this.handleInput}
                        value={this.state.authors}
                        className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                    />
                </div>
                <div className="mt-4">
                    <label>Sort by column</label>
                    <input 
                        type="text"
                        name="sortColumn"
                        placeholder="FIlter by column"
                        onChange={this.handleInput}
                        value={this.state.sortColumn}
                        className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                    />
                </div>
                <div className="mt-4">
                    <label>Sort by direction</label>
                    <input 
                        type="text"
                        name="sortDirection"
                        placeholder="FIlter by Direction"
                        onChange={this.handleInput}
                        value={this.state.sortDirection}
                        className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                    />
                </div>
                <div className="mt-4">
                <input
                  type="submit"
                  value="Search for Books"
                  className="mt-1 p-2 border border-gray-400 rounded cursor-pointer bg-purple-600 text-white"
                />
                </div>
                        </div>
                    </form>
                </section>

                <section className="w-screen m-2 bg-white flex">
                <h1 className="text-lg border-b border-gray-500" style={{fontSize: '30px', marginLeft: '10px', marginTop: '10px'}}>
                    Result of Filter
                </h1>
<table className="table-auto" style={{marginTop: '100px'}}>
  <thead>
    <tr>
      <th className="px-4 py-2">Title</th>
      <th className="px-4 py-2">Description</th>
      <th className="px-4 py-2">Average Review</th>
    </tr>
  </thead>
                <BookList
                    books={
                    this.state.books
                    ? this.state.books
                    : null
                    }
                    >
                </BookList>
            </table>
                </section>
            </div>
        )
    }
}