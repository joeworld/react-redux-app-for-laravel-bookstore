import React, { Component } from 'react'
import Error from "./utils/Error"
import Endpoint from "../services/Endpoint"
import cookie from 'js-cookie'

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
          name: "",
          email: "",
          surname: "",
          password: "",
          password_confirmation: "",
          errors: {}
        };
      }
      handleForm = e => {
        e.preventDefault();
        const data = {
          name: this.state.name,
          email: this.state.email,
          surname: this.state.surname,
          password: this.state.password,
          password_confirmation: this.state.password_confirmation
        };
        Endpoint.register(data)
        .then(res => {
          cookie.set("token", res.data.access_token);
          cookie.set("User", res.data.user);
          this.props.history.push("/profile");
        })
        .catch(e => this.setState({ errors: e.response.data.errors }));
      };
      handleInput = e => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
      };

    render() {
        return (
<div className="flex">
        <div className="w-1/3" />
        <div className="w-1/3 mt-10 p-4 bg-white">
          <form className="border border-gray-500" onSubmit={this.handleForm}>
            <div className="p-4">
              <h1 className="text-lg border-b border-gray-500">Create New Account</h1>
              <div className="mt-4">
                <label>Name</label>
                <input
                  type="text"
                  name="name"
                  placeholder="Your Name"
                  onChange={this.handleInput}
                  className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                />
                <Error
                  error={
                    this.state.errors["name"] ? this.state.errors["name"] : null
                  }
                />
              </div>
              <div className="mt-4">
                <label>Surname</label>
                <input
                  type="text"
                  name="surname"
                  placeholder="Your Surname"
                  onChange={this.handleInput}
                  className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                />
                <Error
                  error={
                    this.state.errors["surname"] ? this.state.errors["surname"] : null
                  }
                />
              </div>
              <div className="mt-4">
                <label>Email</label>
                <input
                  type="email"
                  name="email"
                  placeholder="Your Email"
                  onChange={this.handleInput}
                  className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                />
                <Error
                  error={
                    this.state.errors["email"]
                      ? this.state.errors["email"]
                      : null
                  }
                />
              </div>
              <div className="mt-4">
                <label>Password</label>
                <input
                  type="password"
                  name="password"
                  onChange={this.handleInput}
                  placeholder="Your Password"
                  className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                />
                <Error
                  error={
                    this.state.errors["password"]
                      ? this.state.errors["password"]
                      : null
                  }
                />
              </div>
              <div className="mt-4">
                <label>Confirm Password</label>
                <input
                  type="password"
                  name="password_confirmation"
                  onChange={this.handleInput}
                  placeholder="Confirm your password"
                  className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                />
              </div>
              <div className="mt-4">
                <input
                  type="submit"
                  value="Register"
                  className="mt-1 p-2 border border-gray-400 rounded cursor-pointer bg-purple-600 text-white"
                />
              </div>
            </div>
          </form>
        </div>
      </div>
        )
    }
}