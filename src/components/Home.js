import React, { Component } from 'react'
import Aside from './utils/Aside'

export default class Home extends Component {
    render() {
        return (
            <div className="flex w-full">
                <Aside></Aside>
            </div>
        )
    }
}
