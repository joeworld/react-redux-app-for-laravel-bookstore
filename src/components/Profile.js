import React, { Component } from 'react'
import { connect } from "react-redux"
import Endpoint from "../services/Endpoint"
import Aside from './utils/Aside';
import Error from './utils/Error';

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = { email: props.email, name: props.name, surname:props.surname, user_id: props.user_id, errors: {} };
    }
    
    handleInput = e => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
    };

    handleForm = e => {
        e.preventDefault()
        const data = {
            email: this.state.email,
            name: this.state.name
        }
    
        Endpoint.userUpdate(data)
        .then(res => {
            console.log(res.data)
        })
        .catch(e => this.setState({
            errors: e.response.data.errors
        }))

    }

    render() {
        return (
            <div className="flex w-full">
                <Aside></Aside>
                <section className="w-5/6 m-2 bg-white flex justify-center">
          <form
            className="border border-gray-500 w-1/2 my-5 rounded"
            onSubmit={this.handleForm}
          >
            <div className="p-4">
              <h1 className="text-lg border-b border-gray-500">
                Edit Your Details
              </h1>
              <div className="mt-4">
                <label>Name</label>
                <input
                  type="text"
                  name="name"
                  placeholder="Your Name"
                  onChange={this.handleInput}
                  value={this.state.name}
                  className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                />
                <Error
                error={
                this.state.errors["name"] ? this.state.errors["name"] : null
                }
                >
                </Error>
              </div>
              <div className="mt-4">
                <label>Surname</label>
                <input
                  type="text"
                  name="surname"
                  placeholder="Your Surname"
                  onChange={this.handleInput}
                  value={this.state.name}
                  className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                />
                <Error
                error={
                this.state.errors["surname"] ? this.state.errors["surname"] : null
                }
                >
                </Error>
              </div>
              <div className="mt-4">
                <label>Email</label>
                <input
                  type="email"
                  name="email"
                  placeholder="Your Email"
                  onChange={this.handleInput}
                  value={this.state.email}
                  className="mt-1 p-2 bg-gray-200 rounded border border-gray-400 w-full"
                />
                <Error
                error={
                this.state.errors["email"] ? this.state.errors["email"] : null
                }
                >
                </Error>
              </div>
              <div className="mt-4">
                <input
                  type="submit"
                  value="Update"
                  className="mt-1 p-2 border border-gray-400 rounded cursor-pointer bg-purple-600 text-white"
                />
              </div>
            </div>
          </form>
        </section>
        </div>
        )
    }
}

const mapStateToProps = state => {
    return {
      name: state.auth.user.name,
      email: state.auth.user.email,
      user_id: state.auth.user.id,
      surname: state.auth.surname
    };
  };
  export default connect(
    mapStateToProps,
    null
  )(Profile);