import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import store from './store/index'
import {Provider} from 'react-redux'
import Endpoint from './services/Endpoint'
import ManageToken from './services/ManageToken'
import axios from 'axios'
import cookie from "js-cookie"

const jwt_secret = 'EPjgRNjv0o8i9gshAQFBVdLkFzsKtO7ZBFH6qOw8bw4IFQGciR4L82z3VWAZQ8kY'
var token = cookie.get("token")

if(token){
    token = ManageToken.verifyToken(token, jwt_secret)
}

const render = () => {
    ReactDOM.render(
        <Provider store={store}>
        <App />
        </Provider>,
        document.getElementById('root')
    );
}

if(token){
axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
Endpoint.me()
.then(res => {
    store.dispatch({type:"SET_LOGIN", payload:res.data});
    render()
})
}else{
    render()
}

serviceWorker.unregister();