import React from 'react';
import './css/tailwind.css';
import { BrowserRouter as Router} from "react-router-dom";
import Login from './components/Login';
import Profile from './components/Profile';
import Register from './components/Register';
import Home from './components/Home';
import GuestRoute from './components/utils/GuestRoute';
import AuthRoute from './components/utils/AuthRoute';
import Layout from './components/utils/Layout';
import Books from './components/Books';

function App() {
  return (
    <Router>
      <Layout>
      <div className="bg-gray-300 h-screen">
      <AuthRoute exact path='/' component={Home} />
      <GuestRoute path="/auth/login" component={Login} />
      <GuestRoute path="/auth/register" component={Register} />
      <AuthRoute path="/profile" component={Profile} />
      <AuthRoute path="/books" component={Books} />
      </div>
      </Layout>
    </Router>
  );
}

export default App;