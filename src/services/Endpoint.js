import axios from 'axios'

const baseUrl = 'http://localhost:8000/api'

const Endpoint = {
    getLogin: function(){
        return `${baseUrl}/auth/login`
    },
    login: function(data){
        return axios.post(`${baseUrl}/auth/login`, data)
    },
    register: function(data){
        return axios.post(`${baseUrl}/auth/register`, data)
    },
    me: function(){
        return axios.post(`${baseUrl}/auth/me`)
    },
    userUpdate: function(data){
        return axios.patch(`${baseUrl}/auth/update`, data)
    },
    books: function(params){
        return axios.get(`${baseUrl}/books${params}`)
    }
}

export default Endpoint;