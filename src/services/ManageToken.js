import cookie from "js-cookie"
import jwt from 'jsonwebtoken'
import Endpoint from './Endpoint'

const ManageToken = {
    verifyToken: function (token, jwt_secret){
        jwt.verify(token, jwt_secret, function(err, decoded){
            if(err) {
                token = null;
                cookie.remove("token");
            }else{
                if(decoded.iss !== Endpoint.getLogin()){
                    token = null;
                    cookie.remove("token");
                }
            }
        })
        return token
    },
    removeToken: function() {
        cookie.remove("token");
    }
}

export default ManageToken;